import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DataService } from '../service/data.service';

export interface Project {
	Image: any;
	Title: string;
	Description: string;
};

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

	projects$: Observable<Project[]> | undefined

	constructor(private dataService: DataService) { }

	ngOnInit() {
		this.projects$ = this.dataService.geralAllProjects()
	}

}
