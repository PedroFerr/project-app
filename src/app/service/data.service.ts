import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from 'src/environments/environment';

import { Project } from '../header/header.component';

@Injectable({
	providedIn: 'root'
})
export class DataService {

	apiUrl = environment.apiUrl;

	constructor(private http: HttpClient) { }

	geralAllProjects() {
		this.http.get<Project[]>(`${this.apiUrl}/projects`).subscribe((obj: Project[]) => console.log(obj))

		return this.http.get<Project[]>(`${this.apiUrl}/projects`);
	}

}
