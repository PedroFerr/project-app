import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule  } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppMaterialModule } from './app-material.module';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { AboutComponent } from './about/about.component';

@NgModule({
	declarations: [			
		AppComponent,
      	HeaderComponent,
        AboutComponent
   ],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		HttpClientModule,
		AppMaterialModule,

		AppRoutingModule
	],
	providers: [],
	bootstrap: [AppComponent],
	
})
export class AppModule { }
